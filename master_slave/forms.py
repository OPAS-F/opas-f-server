from django import forms
from oauth2_provider.models import Application


class CreateSlaveForm(forms.ModelForm):
    class Meta:
        model = Application
        fields = ('name',)
