from django.urls import path
from master_slave import views

app_name = "master-slave"


urlpatterns = [
    path('new/', views.CreateSlave.as_view(), name='create-slave'),
    path('', views.ListSlaves.as_view(), name='index'),
    path('<int:pk>/', views.SingleSlave.as_view(), name='single-slave'),
    path('<int:pk>/delete/', views.DeleteSlave.as_view(), name='delete-slave'),
]
