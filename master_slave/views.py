from django.views import generic
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from oauth2_provider.models import Application
from master_slave import forms


class CreateSlave(LoginRequiredMixin, generic.CreateView):
    template_name = "master_slave/pages/create_slave.html"
    form_class = forms.CreateSlaveForm
    success_url = reverse_lazy('master-slave:index')

    def get_queryset(self):
        return Application.objects.filter(user=self.request.user)

    def form_valid(self, form):
        instance = form.instance
        instance.user = self.request.user
        instance.client_type = "confidential"
        instance.authorization_grant_type = "client-credentials"
        instance.save()
        return super(CreateSlave, self).form_valid(form)


class ListSlaves(LoginRequiredMixin, generic.ListView):
    template_name = 'master_slave/pages/index.html'
    context_object_name = "slaves"

    def get_queryset(self):
        return Application.objects.filter(user=self.request.user)


class DeleteSlave(LoginRequiredMixin, generic.DeleteView):
    template_name = "master_slave/pages/delete_slave.html"
    success_url = reverse_lazy('master-slave:index')

    def get_queryset(self):
        return Application.objects.filter(pk=self.kwargs.get('pk'), user=self.request.user)


class SingleSlave(LoginRequiredMixin, generic.DetailView):
    template_name = 'master_slave/pages/single_slave.html'
    context_object_name = "slave"

    def get_queryset(self):
        return Application.objects.filter(user=self.request.user, pk=self.kwargs.get('pk'))
