from rest_framework.test import APITestCase
from django.urls import reverse
from django.utils import timezone
from django.contrib.auth.models import User
from django_dynamic_fixture import G
from oauth2_provider.models import Application, AccessToken
from pentesting.models import Project, UserProject
from master_slave import models
import base64


class SlaveViewsTests(APITestCase):

    def setUp(self):
        self.user = G(User)
        self.user.set_password('password123')
        self.user.save()
        self.project = G(Project)
        G(UserProject, project=self.project, user=self.user)
        application = Application.objects.create(client_type=Application.CLIENT_PUBLIC, name="test_app",
                                                 authorization_grant_type=Application.GRANT_CLIENT_CREDENTIALS,
                                                 user=self.user)
        self.access_token = AccessToken.objects.create(user=self.user, scope="read write",
                                                       expires=timezone.now() + timezone.timedelta(seconds=300),
                                                       token="secret-access-token-key", application=application)

    def test_slave_auth(self):
        url = reverse('api:master-slave:oauth2_provider:token')
        application = Application.objects.create(client_type=Application.CLIENT_PUBLIC, name="test_app",
                                                 authorization_grant_type=Application.GRANT_CLIENT_CREDENTIALS,
                                                 user=self.user)
        auth_str = "%s:%s" % (application.client_id, application.client_secret)
        auth = base64.b64encode(auth_str.encode('utf-8'))
        auth_header = {'HTTP_AUTHORIZATION': 'Basic ' + auth.decode('utf-8')}
        data = {'grant_type': 'client_credentials'}
        response = self.client.post(url, data=data, **auth_header)
        self.assertEqual(response.status_code, 200)
        self.assertEqual("access_token" in response.json(), True)

    def test_fetch_queue(self):
        for i in range(3):
            G(models.Queue, user=self.user, project=self.project, start_date=timezone.now().date())
        url = reverse('api:master-slave:fetch-queue')
        self.client.credentials(HTTP_AUTHORIZATION='Bearer %s' % self.access_token)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json().get('results')), 3)
        G(models.Queue, user=self.user, project=self.project, finished=True)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json().get('results')), 3)
        G(models.Queue, user=self.user,project=self.project, start_date=timezone.now().date() + timezone.timedelta(days=2))
        G(models.Queue, user=self.user, project=self.project, finished=True)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json().get('results')), 3)

    def test_fetch_queue_forbidden(self):
        for i in range(3):
            G(models.Queue, user=self.user, project=self.project, start_date=timezone.now().date())
        G(models.Queue)
        self.client.credentials(HTTP_AUTHORIZATION='Bearer %s' % self.access_token)
        url = reverse('api:master-slave:fetch-queue')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json().get('results')), 3)

    def test_queue_item_in_progress(self):
        queue_item = G(models.Queue, user=self.user, project=self.project, in_progress=False)
        self.client.credentials(HTTP_AUTHORIZATION='Bearer %s' % self.access_token)
        url = reverse('api:master-slave:queue-item-in-progress', kwargs={'pk': queue_item.pk})
        response = self.client.patch(url, data={'in_progress': True})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(models.Queue.objects.filter(in_progress=True).count(), 1)

    def test_queue_item_in_progress_forbidden(self):
        queue_item = G(models.Queue, in_progress=False)
        self.client.credentials(HTTP_AUTHORIZATION='Bearer %s' % self.access_token)
        url = reverse('api:master-slave:queue-item-in-progress', kwargs={'pk': queue_item.pk})
        response = self.client.patch(url, data={'in_progress': True})
        self.assertEqual(response.status_code, 403)
        self.assertEqual(models.Queue.objects.filter(in_progress=True).count(), 0)

    def test_queue_item_finished(self):
        queue_item = G(models.Queue, user=self.user, project=self.project, finished=False)
        self.client.credentials(HTTP_AUTHORIZATION='Bearer %s' % self.access_token)
        url = reverse('api:master-slave:queue-item-finished', kwargs={'pk': queue_item.pk})
        response = self.client.patch(url, data={'finished': True})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(models.Queue.objects.filter(finished=True).count(), 1)


    def test_queue_item_finished_forbidden(self):
        queue_item = G(models.Queue, finished=False)
        self.client.credentials(HTTP_AUTHORIZATION='Bearer %s' % self.access_token)
        url = reverse('api:master-slave:queue-item-finished', kwargs={'pk': queue_item.pk})
        response = self.client.patch(url, data={'finished': True})
        self.assertEqual(response.status_code, 403)
        self.assertEqual(models.Queue.objects.filter(finished=True).count(), 0)

    def test_applications_create(self):
        pass

    def test_applications_update(self):
        pass

    def test_applications_update_forbidden(self):
        pass

    def test_application_delete(self):
        pass

    def test_application_delete_forbidden(self):
        pass

    def test_command_started(self):
        pass

    def test_command_ended(self):
        pass
