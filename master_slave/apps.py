from django.apps import AppConfig


class MasterSlaveConfig(AppConfig):
    name = 'master_slave'
