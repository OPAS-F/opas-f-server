#!/bin/sh

set -e

if [ "${1:0:1}" == '-' ]; then
  set -- tor $@
fi

if [ "$1" == "tor" ]; then
  # Set config
  python3 /docker/tor_config.py

  # set rights on keys
  chown -R tor:tor /var/lib/tor/hidden_service/
  chmod -R 700 /var/lib/tor/hidden_service/

  # Switch user

  set -- su tor -s /bin/sh -c "$@"
fi

exec "$@"