#!/usr/bin/python3

import os
import time, sys
import logging
from subprocess import STDOUT, check_output
# Generate conf for tor hidden service

# TOOD: support multiple ports and hosts


def get_hidden_service_environs():
    services = []
    #for env in os.environ:
    #    if env.startswith("HIDDEN_SERVICE"):
    #        services.append(env.replace("HIDDEN_SERVICE_", ""))
    services.append(os.environ.get('HIDDEN_SERVICE_NAME'))
    return services


def build_hidden_service_obj(service_name):
    port = os.environ.get('HIDDEN_SERVICE_PORT')
    return {'name': service_name, 'port': str(port)}


def set_conf():
    all_hidden_services = []
    links = get_hidden_service_environs()
    logging.info("Links:%s", links)
    with open("/etc/tor/torrc", "w") as conf:
        counter = 0
        while len(links) < 1:
            if counter > 5:
                print("no links found")
                sys.exit(1)
            counter += 1
            time.sleep(3)
        for link in links:
            hidden_service = build_hidden_service_obj(link)
            path = "/var/lib/tor/hidden_service/{service}".format(service=hidden_service.get('name'))
            conf.write('HiddenServiceDir {path}\n'.format(path=path))
            conf.write('HiddenServiceVersion 3\n')
            all_hidden_services.append(hidden_service.get('name'))
            conf_line = '%s %s:%s' % (hidden_service.get('port'), hidden_service.get('name'), hidden_service.get('port'))
            conf.write('HiddenServicePort {service}\n'.format(
                service=conf_line
            ))
        # Disable local socket
        conf.write("SocksPort 0\n")
    return all_hidden_services


def gen_host(services):
    try:
        output = check_output("tor", stderr=STDOUT, timeout=5)
    except:
        pass
    for service in services:
        filename = "/var/lib/tor/hidden_service/{service}/hostname".format(
            service=service
        )
        with open(filename, 'r') as hostfile:
            onion=hostfile.read()
            print('{onion}'.format(
                onion=onion
            ))


if __name__ == '__main__':
    services = set_conf()
    gen_host(services)
