#!/bin/sh

# start crond
echo Starting cron-daemon
crond -b

python3 manage.py migrate --noinput
python3 manage.py initadmin
python3 manage.py collectstatic --noinput

# Start Gunicorn processes
echo Starting Gunicorn.
gunicorn opas_f_server.wsgi:application \
    --bind 0.0.0.0:8000 \
    --workers 3
