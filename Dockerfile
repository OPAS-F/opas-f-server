FROM frolvlad/alpine-python3

WORKDIR /opasf_server

RUN apk update && apk add gcc python3-dev musl-dev dcron libffi-dev postgresql-dev zlib-dev jpeg-dev freetype-dev

ADD requirements.txt /requirements.txt
RUN pip install --upgrade pip
RUN pip install -r /requirements.txt
RUN pip install django[argon2]


ADD docker/start_gunicorn.sh /start_gunicorn.sh
RUN chmod +x /start_gunicorn.sh

ADD . /opasf_server

RUN chmod +x /opasf_server/disclose_vulnerabilities.py
RUN echo '* * * * * python3 /opasf_server/disclose_vulnerabilities.py >> /bbv_disclosure.log 2>&1 \n' > /var/spool/cron/crontabs/root

ENTRYPOINT [ "/start_gunicorn.sh" ]