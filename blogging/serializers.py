from rest_framework import serializers
from blogging import models
from opas_f_server.exceptions.api import ForbiddenAPIException


class BlogSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Blog
        exclude = ('id', )
        read_only_fields = ('user', 'slug')


class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Post
        exclude = ('id',)
        read_only_fields = ('created_at', 'slug')

    def validate_blog(self, value):
        user = self.context['request'].user
        project = models.Blog.objects.filter(user=user)
        if not project.exists():
            raise ForbiddenAPIException()
        return value


class BlogRetrieveSerializer(serializers.ModelSerializer):
    posts = PostSerializer(many=True, read_only=True, source='post_set')

    class Meta:
        model = models.Blog
        exclude = ('id',)
        read_only_fields = ('user', 'slug', 'posts')
