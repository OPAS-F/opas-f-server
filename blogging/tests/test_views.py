from rest_framework.test import APITestCase
from django_dynamic_fixture import G
from django.contrib.auth.models import User
from django.urls import reverse
from blogging import models


class BlogViewTests(APITestCase):
    def setUp(self):
        self.user = G(User)
        self.client.force_login(user=self.user)

    def test_create_blog(self):
        data = {'title': 'just a test blog'}
        url = reverse('api:blogging:blog-list')
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json().get('title'), data['title'])

    def test_create_blog_forbidden(self):
        self.client.logout()
        data = {'title': 'host a test blog'}
        url = reverse('api:blogging:blog-list')
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 403)

    def test_update_blog(self):
        blog = G(models.Blog, user=self.user)
        url = reverse('api:blogging:blog-detail', kwargs={'pk': blog.pk})
        data = {'title': 'new title'}
        response = self.client.patch(url, data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json().get('title'), data['title'])

    def test_update_blog_forbidden(self):
        blog = G(models.Blog)
        url = reverse('api:blogging:blog-detail', kwargs={'pk': blog.pk})
        data = {'title': 'new title'}
        response = self.client.patch(url, data)
        self.assertEqual(response.status_code, 404)

    def test_delete_blog(self):
        blog = G(models.Blog, user=self.user)
        url = reverse('api:blogging:blog-detail', kwargs={'pk': blog.pk})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 204)
        self.assertEqual(models.Blog.objects.count(), 0)

    def test_delete_blog_forbidden(self):
        blog = G(models.Blog)
        url = reverse('api:blogging:blog-detail', kwargs={'pk': blog.pk})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 404)

    def test_read_blog(self):
        blog = G(models.Blog)
        url = reverse('api:blogging:blog-retrieve-detail', kwargs={'slug': blog.slug})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json().get('title'), blog.title)

    def test_read_post(self):
        post = G(models.Post)
        url = reverse('api:blogging:post-retrieve-detail', kwargs={'slug': post.slug})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json().get('post'), post.post)

    def test_create_post(self):
        blog = G(models.Blog, user=self.user)
        data = {'blog': blog.pk, 'title': 'welcome', 'post': 'lorem ipsum'}
        url = reverse('api:blogging:post-list')
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(models.Post.objects.count(), 1)
        self.assertEqual(models.Blog.objects.first().post_set.count(), 1)

    def test_create_post_forbidden(self):
        blog = G(models.Blog)
        data = {'blog': blog.pk, 'title': 'welcome', 'post': 'lorem ipsum'}
        url = reverse('api:blogging:post-list')
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 403)

    def test_update_post(self):
        blog = G(models.Blog, user=self.user)
        post = G(models.Post, blog=blog)
        data = {'title': 'new title'}
        url = reverse('api:blogging:post-detail', kwargs={'pk': post.pk})
        response = self.client.patch(url, data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json().get('title'), data['title'])

    def test_update_post_forbidden(self):
        blog = G(models.Blog)
        post = G(models.Post, blog=blog)
        data = {'title': 'new title'}
        url = reverse('api:blogging:post-detail', kwargs={'pk': post.pk})
        response = self.client.patch(url, data)
        self.assertEqual(response.status_code, 404)

    def test_delete_post(self):
        blog = G(models.Blog, user=self.user)
        post = G(models.Post, blog=blog)
        url = reverse('api:blogging:post-detail', kwargs={'pk': post.pk})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 204)
        self.assertEqual(models.Post.objects.count(), 0)

    def test_delete_post_forbidden(self):
        blog = G(models.Blog)
        post = G(models.Post, blog=blog)
        url = reverse('api:blogging:post-detail', kwargs={'pk': post.pk})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 404)
