from rest_framework import viewsets, mixins
from opas_f_server.permissions import IsOwner
from blogging import serializers
from blogging import models


class BlogViewSet(mixins.UpdateModelMixin, mixins.CreateModelMixin, mixins.DestroyModelMixin, viewsets.GenericViewSet):
    permission_classes = [IsOwner]
    serializer_class = serializers.BlogSerializer

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def get_queryset(self):
        return models.Blog.objects.filter(user=self.request.user, pk=self.kwargs.get('pk'))


class BlogRetrieve(mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    serializer_class = serializers.BlogRetrieveSerializer
    lookup_field = "slug"

    def get_queryset(self):
        return models.Blog.objects.filter(slug=self.kwargs.get('slug'))


class PostViewSet(mixins.UpdateModelMixin, mixins.CreateModelMixin, mixins.DestroyModelMixin, viewsets.GenericViewSet):
    permission_classes = [IsOwner]
    serializer_class = serializers.PostSerializer

    def get_queryset(self):
        return models.Post.objects.filter(blog__user=self.request.user, pk=self.kwargs.get('pk'))


class PostRetrieve(mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    serializer_class = serializers.PostSerializer
    lookup_field = "slug"

    def get_queryset(self):
        return models.Post.objects.filter(slug=self.kwargs.get('slug'))
