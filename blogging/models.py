from django.db import models
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify
from markdownx.models import MarkdownxField
from markdownx.utils import markdownify
import bleach


# Create your models here.


class Blog(models.Model):
    """ database model for user blog """
    user = models.OneToOneField(User, on_delete=models.CASCADE, unique=True)
    title = models.CharField(max_length=32)
    slug = models.SlugField()

    def __str__(self):
        return "%s (%s)" % (self.title, self.user.username)

    def save(self, *args, **kwargs):
        if not self.pk:
            self.slug = slugify("%s %s" % (self.user.username, self.title))
        super(Blog, self).save(*args, **kwargs)

    @property
    def owner(self):
        return self.user


class Post(models.Model):
    blog = models.ForeignKey(Blog, on_delete=models.CASCADE)
    title = models.CharField(max_length=32, unique=True)
    post = MarkdownxField()
    slug = models.SlugField(unique=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.slug

    def save(self, *args, **kwargs):
        if not self.pk:
            self.slug = slugify(self.title)
        self.post = bleach.clean(self.post)
        super(Post, self).save(*args, **kwargs)

    @property
    def owner(self):
        return self.blog.user

    @property
    def formatted_markdown(self):
        self.post = bleach.clean(self.post)
        return markdownify(self.post)

    class Meta:
        ordering = ('-created_at',)
