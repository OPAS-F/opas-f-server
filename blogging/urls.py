from rest_framework.routers import DefaultRouter
from blogging import views

app_name = "blogging"

router = DefaultRouter()
router.register(r'posts', views.PostViewSet, basename='post')
router.register(r'b', views.BlogRetrieve, basename='blog-retrieve')
router.register(r'p', views.PostRetrieve, basename='post-retrieve')
router.register(r'', views.BlogViewSet, basename='blog')

urlpatterns = router.urls
