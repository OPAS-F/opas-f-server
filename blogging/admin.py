from django.contrib import admin
from blogging import models

# Register your models here.
admin.site.register(models.Blog)
admin.site.register(models.Post)
