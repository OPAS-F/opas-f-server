""" disclose all bug bounty vulnerabilities
that reached deadline
"""
import os
import logging
import importlib

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "opas_f_server.settings")
import django
django.setup()
from django.utils import timezone
from django.conf import settings
from django.template.loader import render_to_string
from bughunting.models import Bug


logging.basicConfig(level=logging.INFO)


def load_bots(vuln):
    for bot in settings.BUG_DISCLOSURE_BOTS:
        if bot.get('enabled') is True:
            loaded_module = importlib.import_module(bot.get('module'))
            new_bot = loaded_module.DisclosureBot(**bot.get('args'))
            domain = settings.SELF_DOMAIN
            message = render_to_string(bot.get('args').get('message_template'),
                                       {'domain': domain, 'bug': vuln})
            new_bot.disclosed(message, vuln)


if __name__ == '__main__':
    vulns = Bug.objects.filter(
        bugmeta__is_disclosed=False, bugmeta__approved=True,
        bugmeta__date_disclosure__lte=timezone.now()
    )

    for vuln in vulns:
        vuln.bugmeta.is_disclosed = True
        vuln.bugmeta.date_disclosed = timezone.now().date()
        vuln.bugmeta.save()
        logging.info('disclosed %s', vuln)
        load_bots(vuln)

