from django.urls import path
from public.api import views


app_name = 'public'

urlpatterns = [
    path('statistics/bughunting/patched-unpatched/',
         views.PatchedUnpatchedStatistic.as_view(), name='statistics-bughunting-patched-unpatched'),
    path('statistics/bughunting/top-bug-categories/',
         views.TopBugCategoriesStatistic.as_view(), name='statistics-bughunting-top-bug-categories'),
]
