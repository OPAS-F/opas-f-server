from rest_framework import generics
from rest_framework.response import Response
from django.db.models import Count
from bughunting.models import Bug


class PatchedUnpatchedStatistic(generics.RetrieveAPIView):

    def retrieve(self, request, *args, **kwargs):
        bugs = Bug.objects.filter(bugmeta__approved=True)
        patched = bugs.filter(bugmeta__is_fixed=True).count()
        unpatched = bugs.filter(bugmeta__is_fixed=False, bugmeta__is_disclosed=True).count()
        awaiting_response = bugs.filter(bugmeta__is_disclosed=False).count()
        data = {'patched': patched, 'unpatched': unpatched, 'awaiting_response': awaiting_response}
        return Response(data)


class TopBugCategoriesStatistic(generics.RetrieveAPIView):
    def retrieve(self, request, *args, **kwargs):
        return Response(Bug.objects.filter(bugmeta__approved=True).values('name').annotate(
            y=Count('pk')).order_by('-y')[:5])
