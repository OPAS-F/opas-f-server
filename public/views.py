from django.views import generic
from bughunting.models import Bug


class IndexPage(generic.TemplateView):
    template_name = 'public/index.html'

    def get_domain_count(self):
        return Bug.objects.filter(bugmeta__approved=True).values('domain').distinct().count()

    def get_reported_bugs_count(self):
        return Bug.objects.filter(bugmeta__approved=True).count()

    def get_latest_bug_submissions(self):
        return Bug.objects.filter(bugmeta__approved=True).order_by('-bugmeta__date_sent')[:5]

    def get_latest_patched_bugs(self):
        return Bug.objects.filter(bugmeta__approved=True, bugmeta__is_fixed=True).order_by('-bugmeta__date_fixed')[:5]

    def get_context_data(self, **kwargs):
        context = super(IndexPage, self).get_context_data(**kwargs)
        context['domain_count'] = self.get_domain_count()
        context['reported_bugs_count'] = self.get_reported_bugs_count()
        context['latest_bug_submissions'] = self.get_latest_bug_submissions()
        context['latest_patched_bugs'] = self.get_latest_patched_bugs()
        return context
