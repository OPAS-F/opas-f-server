from django.urls import path
from public import views


app_name = 'public'


urlpatterns = [
    path('', views.IndexPage.as_view(), name='index-page')
]
