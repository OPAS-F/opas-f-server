from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse
import uuid
from pinax.badges.registry import badges
from bughunting import badges as my_badges
# Create your models here.


class Bug(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, primary_key=True)
    name = models.CharField(max_length=128)
    domain = models.URLField()
    path = models.CharField(max_length=1024)
    parameter = models.CharField(max_length=64)
    proof_of_concept = models.TextField()
    method = models.CharField(max_length=8, default="GET")
    description = models.TextField(blank=True, null=True)

    @property
    def owner(self):
        return self.bugmeta.user

    def __str__(self):
        return "%s on %s" % (self.name, self.domain)

    def get_absolute_url(self):
        return reverse('bughunting:single-bug', kwargs={'pk': self.pk})

    class Meta:
        unique_together = (('domain', 'parameter', 'path', 'name'),)


class BugMeta(models.Model):
    bug = models.OneToOneField(Bug, on_delete=models.CASCADE, primary_key=True)
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    is_fixed = models.BooleanField(default=False)
    date_fixed = models.DateField(blank=True, null=True)
    date_discovered = models.DateTimeField(auto_now_add=True)
    is_disclosed = models.BooleanField(default=False)
    date_disclosure = models.DateField(blank=True, null=True, help_text="date when bug becomes auto disclosed")
    date_disclosed = models.DateField(blank=True, null=True)
    approved = models.BooleanField(default=False)
    contact_email = models.EmailField()
    is_sent = models.BooleanField(default=False)
    is_draft = models.BooleanField(default=False)
    date_sent = models.DateTimeField(blank=True, null=True)
    send_email_copy_user = models.BooleanField(default=False)


class Comment(models.Model):
    bug = models.ForeignKey(Bug, on_delete=models.CASCADE)
    text = models.TextField()
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    date = models.DateTimeField(auto_now_add=True)


# Register Badges here
badges.register(my_badges.SecuredWebsitesBadge)
badges.register(my_badges.PatchedBadge)