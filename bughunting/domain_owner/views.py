from django.views import generic
from django.urls import reverse_lazy
from bughunting.forms import WriteCommentForm
from bughunting.utils.tokens.token import validate_access_token


class WriteBugCommentAccessToken(generic.CreateView):
    form_class = WriteCommentForm
    http_method_names = ["post"]

    def get_queryset(self):
        is_domain_owner, queryset = validate_access_token(self.kwargs.get('pk'), self.kwargs.get('token'))
        return queryset

    def get_success_url(self):
        return reverse_lazy('bughunting:domain-owner:bug-details', kwargs={'pk': self.kwargs.get('pk'),
                                                                           'token': self.kwargs.get('token')})

    def form_valid(self, form):
        form.instance.bug = self.get_object()
        return super(WriteBugCommentAccessToken, self).form_valid(form)


class OwnerBugDetails(generic.DetailView):
    template_name = 'bughunting/pages/single_bug_page.html'
    context_object_name = "bug"

    def get_context_data(self, **kwargs):
        is_domain_owner, queryset = validate_access_token(self.kwargs.get('pk'), self.kwargs.get('token'))
        context = super(OwnerBugDetails, self).get_context_data(**kwargs)
        context.update({
            'is_domain_owner': is_domain_owner, 'token': self.kwargs.get('token'),
            'write_comment_form': WriteCommentForm()
        })
        return context

    def get_queryset(self):
        is_domain_owner, queryset = validate_access_token(self.kwargs.get('pk'), self.kwargs.get('token'))
        if queryset:
            queryset = queryset.filter(bugmeta__approved=True)
        return queryset
