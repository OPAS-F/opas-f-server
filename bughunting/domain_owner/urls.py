from django.urls import path
from bughunting.domain_owner import views

app_name = "domain-owner"


urlpatterns = [
    path('<str:pk>/<str:token>/', views.OwnerBugDetails.as_view(),
         name='bug-details'),

    path('<str:pk>/<str:token>/comments/new/', views.WriteBugCommentAccessToken.as_view(),
         name='write-comment')
]