from pinax.badges.base import Badge, BadgeAwarded, BadgeDetail

from bughunting import models


class SecuredWebsitesBadge(Badge):
    slug = "Secured Websites"
    levels = [
        BadgeDetail(name="Bronze", description="+10 Websites"),
        BadgeDetail(name="Silver", description="+100 Websites"),
        BadgeDetail(name="Gold", description="+500 Websites"),
        BadgeDetail(name="Master", description="+1000 Websites")
    ]
    events = ["secured_websites_awarded"]
    multiple = False

    def award(self, **state):
        user = state.get('user')
        bugs = models.Bug.objects.filter(bugmeta__user=user, bugmeta__is_sent=True).count()
        if bugs > 1000:
            return BadgeAwarded(level=4)
        elif bugs > 500:
            return BadgeAwarded(level=3)
        elif bugs > 100:
            return BadgeAwarded(level=2)
        elif bugs > 10:
            return BadgeAwarded(level=1)


class PatchedBadge(Badge):
    slug = "Patch"
    levels = [
        BadgeDetail(name="Bronze", description="20% patched"),
        BadgeDetail(name="Silver", description="50% patched"),
        BadgeDetail(name="Gold", description="80% patched"),
        BadgeDetail(name="Master", description="100% patched")
    ]
    events = ["patched_websites_awarded"]
    multiple = False

    def award(self, **state):
        user = state.get('user')
        bugs = models.Bug.objects.filter(bugmeta__user=user, bugmeta__approved=True)
        all_bugs = bugs.count()
        fixed_bugs = bugs.filter(bugmeta__is_fixed=True).count()
        percent = 100 * fixed_bugs / all_bugs
        if percent >= 100:
            return BadgeAwarded(level=4)
        elif percent >= 80:
            return BadgeAwarded(level=3)
        elif percent >= 50:
            return BadgeAwarded(level=2)
        elif percent >= 1:
            return BadgeAwarded(level=1)
