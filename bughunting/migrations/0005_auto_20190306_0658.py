# Generated by Django 2.1.7 on 2019-03-06 06:58

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bughunting', '0004_auto_20190306_0651'),
    ]

    operations = [
        migrations.RenameField(
            model_name='bugmeta',
            old_name='date_send',
            new_name='date_sent',
        ),
    ]
