from django.test import TestCase
from django.urls import reverse_lazy
from django.contrib.auth.models import User
from django_dynamic_fixture import G
from bughunting import models
from bughunting.utils.tokens.bug_token_generator import bug_access_token_generator


class BughuntingViewsTestCase(TestCase):
    def setUp(self):
        self.user = G(User, password="test")

    def test_bug_details_owner_token(self):
        bug = G(models.Bug)
        G(models.BugMeta, bug=bug, is_sent=True, is_draft=False, approved=True, is_disclosed=False)
        token = bug_access_token_generator.make_token(bug)
        url = reverse_lazy('bughunting:domain-owner:bug-details', kwargs={'pk': bug.pk, 'token': token})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual("Details hidden until disclosure!" in response.content.decode('utf-8'), False)

    def test_bug_details_owner_not_approved_bug(self):
        bug = G(models.Bug)
        G(models.BugMeta, bug=bug, is_sent=True, approved=False)
        token = bug_access_token_generator.make_token(bug)
        url = reverse_lazy('bughunting:domain-owner:bug-details', kwargs={'pk': bug.pk, 'token': token})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_bug_details_owner_invalid_token(self):
        bug = G(models.Bug)
        G(models.BugMeta, bug=bug, is_sent=True, approved=True)
        token = "randomtoken"
        url = reverse_lazy('bughunting:domain-owner:bug-details', kwargs={'pk': bug.pk, 'token': token})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_bug_details_owner_draft_bug(self):
        bug = G(models.Bug)
        G(models.BugMeta, bug=bug, is_sent=False, approved=False, is_draft=True)
        token = "randomtoken"
        url = reverse_lazy('bughunting:domain-owner:bug-details', kwargs={'pk': bug.pk, 'token': token})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_all_bugs(self):
        # TODO: implement
        # TODO: check if drafts and not approved bugs are hidden and no details were leaked
        pass

    def test_single_bug(self):
        bug = G(models.Bug)
        G(models.BugMeta, bug=bug, is_sent=True, approved=True)
        url = reverse_lazy('bughunting:single-bug', kwargs={'pk': bug.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual("Details hidden until disclosure!" in response.content.decode('utf-8'), True)

    def test_view_other_users_draft_bug(self):
        """ ensure that we cannot read draft bugs of other users """
        bug = G(models.Bug)
        G(models.BugMeta, bug=bug, is_draft=True, approved=False)
        url = reverse_lazy('bughunting:single-bug', kwargs={'pk': bug.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_drafts_not_marked_approved(self):
        self.client.force_login(user=self.user)
        data = {'name': 'testbug', 'domain': 'http://example.test', 'path': "/index.php", 'parameter': 'id',
                'proof_of_concept': 'asdf', 'method': 'GET', 'description': 'aaaa',
                'contact_email': 'admin@example.com', 'is_draft': True}
        url = reverse_lazy('bughunting:submit-bug')
        response = self.client.post(url, data, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(models.Bug.objects.count(), 1)
        self.assertEqual(models.Bug.objects.filter(bugmeta__approved=False).count(), 1)

    def test_write_bug_comments(self):
        bug = G(models.Bug)
        self.client.force_login(user=self.user)
        G(models.BugMeta, bug=bug, user=self.user)
        url = reverse_lazy('bughunting:write-comment', kwargs={'pk': bug.pk})
        data = {'text': 'some comment'}
        response = self.client.post(url, data, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(models.Comment.objects.count(), 1)

    def test_write_bug_comments_forbidden(self):
        user2 = G(models.User)
        bug = G(models.Bug)
        G(models.BugMeta, bug=bug, user=user2)
        url = reverse_lazy('bughunting:write-comment', kwargs={'pk': bug.pk})
        data = {'text': 'forbidden comment'}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(models.Comment.objects.count(), 0)

    def test_write_bug_comment_access_token(self):
        bug = G(models.Bug)
        G(models.BugMeta, bug=bug, approved=True, is_sent=True, is_draft=False)
        token = bug_access_token_generator.make_token(bug)
        url = reverse_lazy('bughunting:domain-owner:write-comment', kwargs={'pk': bug.pk, 'token': token})
        data = {'text': 'some comment'}
        response = self.client.post(url, data, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(models.Comment.objects.count(), 1)
        self.assertEqual(models.Comment.objects.filter(bug=bug).count(), 1)

    def test_write_bug_comment_access_token_forbidden(self):
        bug = G(models.Bug)
        G(models.BugMeta, bug=bug, approved=True, is_sent=True, is_draft=False)
        token = "invalid"
        url = reverse_lazy('bughunting:domain-owner:write-comment', kwargs={'pk': bug.pk, 'token': token})
        data = {'text': 'forbidden comment'}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 404)
        self.assertEqual(models.Comment.objects.count(), 0)
