from rest_framework.test import APITestCase
from django_dynamic_fixture import G
from django.contrib.auth.models import User
from django.urls import reverse
from django.core import mail
from django.utils import timezone
from django.test.utils import override_settings
from bughunting import models


class BughuntingAPITests(APITestCase):

    def setUp(self):
        self.user = G(User)
        self.client.force_login(user=self.user)

    def test_create_bug_auto_approval(self):
        with override_settings(BUGHUNTING_NEED_MANUAL_APPROVING=False):
            url = reverse('api:bughunting:user-bug-list')
            data = {
                'domain': 'https://example.com', 'parameter': 'page', 'name': "SQL-Injection",
                'path': '/index.php', 'proof_of_concept': 'test poc', 'bug_info': {'contact_email': 'admin@example.com'}
            }
            response = self.client.post(url, data, format='json')
            self.assertEqual(response.status_code, 201)
            self.assertEqual(models.Bug.objects.count(), 1)
            self.assertEqual(models.Bug.objects.filter(bugmeta__approved=True).count(), 1)
            self.assertEqual(len(mail.outbox), 1)

    def test_create_bug_manual_approval(self):
        with override_settings(BUGHUNTING_NEED_MANUAL_APPROVING=True):
            url = reverse('api:bughunting:user-bug-list')
            data = {
                'domain': 'https://example.com', 'parameter': 'page', 'name': "SQL-Injection",
                'path': '/index.php', 'proof_of_concept': 'test poc', 'bug_info': {'contact_email': 'admin@example.com'}
            }
            response = self.client.post(url, data, format='json')
            self.assertEqual(response.status_code, 201)
            self.assertEqual(models.Bug.objects.count(), 1)
            self.assertEqual(models.Bug.objects.filter(bugmeta__approved=False).count(), 1)
            self.assertEqual(len(mail.outbox), 0)

    def test_list_user_bugs(self):
        url = reverse('api:bughunting:user-bug-list')
        for i in range(3):
            bug = G(models.Bug)
            G(models.BugMeta, user=self.user, bug=bug, approved=True, is_draft=False, is_sent=True)
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json().get('count'), 3)

    def test_list_user_bugs_forbidden(self):
        url = reverse('api:bughunting:user-bug-list')
        user2 = G(User)
        bug = G(models.Bug)
        G(models.BugMeta, user=user2, bug=bug)
        for i in range(3):
            bug = G(models.Bug)
            G(models.BugMeta, user=self.user, bug=bug)
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json().get('count'), 3)

    def test_list_all_bugs(self):
        url = reverse('api:bughunting:bug-list')
        hidden_bug = G(models.Bug)
        user2 = G(User)
        G(models.BugMeta, user=user2, bug=hidden_bug, is_sent=False, approved=False)
        for i in range(10):
            bug = G(models.Bug)
            G(models.BugMeta, user=self.user, bug=bug, is_sent=True, approved=True)
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json().get('count'), 10)

    def test_list_all_bugs_forbidden(self):
        """ check if not disclosed bugs details are hidden """
        url = reverse('api:bughunting:bug-list')
        hidden_bug = G(models.Bug)
        user2 = G(User)
        G(models.BugMeta, user=user2, bug=hidden_bug, is_sent=True, approved=True)
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, 200)
        results = response.json().get('results')
        self.assertEqual("parameter" in results[0], False)

    def test_user_bug_details(self):
        bug = G(models.Bug)
        G(models.BugMeta, user=self.user, is_sent=True, bug=bug, approved=True)
        url = reverse('api:bughunting:user-bug-detail', kwargs={'pk': bug.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json().get('uuid'), str(bug.pk))

    def test_user_bug_details_disallowed(self):
        bug = G(models.Bug)
        user2 = G(User)
        G(models.BugMeta, user=user2, is_sent=False, bug=bug)
        url = reverse('api:bughunting:user-bug-detail', kwargs={'pk': bug.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_resend_notification_mail(self):
        bug = G(models.Bug)
        G(models.BugMeta, is_sent=True, user=self.user, bug=bug, approved=True)
        url = reverse('api:bughunting:user-bug-resend-notification-mail', kwargs={'pk': bug.pk})
        response = self.client.post(url, {})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(mail.outbox), 1)

    def test_increase_date_disclosure(self):
        bug = G(models.Bug)
        old_date = timezone.now().date()
        new_date = old_date + timezone.timedelta(days=3)
        G(models.BugMeta, user=self.user, bug=bug, date_disclosure=old_date)
        url = reverse('api:bughunting:increase-disclosure-deadline', kwargs={'pk': bug.pk})
        data = {'days': 3}
        response = self.client.patch(url, data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json().get('bug_info').get('date_disclosure'), str(new_date))
        bug2 = G(models.Bug)
        user2 = G(User)
        G(models.BugMeta, user=user2, bug=bug2)
        url = reverse('api:bughunting:increase-disclosure-deadline', kwargs={'pk': bug2.pk})
        data = {'days': 3}
        response = self.client.patch(url, data)
        self.assertEqual(response.status_code, 403)

    def test_read_bug_not_disclosed(self):
        user2 = G(User)
        bug = G(models.Bug)
        G(models.BugMeta, is_sent=True, bug=bug, user=user2, is_disclosed=False, approved=True)
        url = reverse('api:bughunting:bug-detail', kwargs={'pk': bug.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json().get('name'), bug.name)
        self.assertEqual(response.json().get('domain'), bug.domain)
        self.assertEqual("path" not in response.json(), True)
        self.assertEqual("parameter" not in response.json(), True)
        self.assertEqual("proof_of_concept" not in response.json(), True)
        self.assertEqual("method" not in response.json(), True)
        self.assertEqual("description" not in response.json(), True)
        self.assertEqual("contact_email" not in response.json().get('bug_info'), True)

    def test_read_bug_disclosed(self):
        user2 = G(User)
        bug = G(models.Bug)
        G(models.BugMeta, is_sent=True, bug=bug, user=user2, is_disclosed=True, approved=True)
        url = reverse('api:bughunting:bug-detail', kwargs={'pk': bug.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json().get('name'), bug.name)
        self.assertEqual(response.json().get('domain'), bug.domain)
        self.assertEqual("path" in response.json(), True)
        self.assertEqual("parameter" in response.json(), True)
        self.assertEqual("proof_of_concept" in response.json(), True)
        self.assertEqual("method" in response.json(), True)
        self.assertEqual("description" in response.json(), True)
        self.assertEqual("contact_email" not in response.json().get('bug_info'), True)
