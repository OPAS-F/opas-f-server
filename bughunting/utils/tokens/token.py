""" Access Token Utils """
from bughunting.models import Bug
from bughunting.utils.tokens.bug_token_generator import bug_access_token_generator


def validate_access_token(bug_id, token):
    tmp_vuln = Bug.objects.none()
    is_domain_owner = None
    try:
        tmp_vuln = Bug.objects.filter(pk=bug_id, bugmeta__is_sent=True, bugmeta__is_disclosed=False,
                                      bugmeta__approved=True)
        if tmp_vuln.exists() and bug_access_token_generator.check_token(tmp_vuln.get(), token):
            is_domain_owner = True
        else:
            tmp_vuln = Bug.objects.none()
    except (TypeError, ValueError, OverflowError) as err:
        print(err)
    except Exception as err:
        print(err)
    return is_domain_owner, tmp_vuln
