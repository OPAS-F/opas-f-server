""" helpers for bug bounty mode app"""
from django.utils import timezone
from django.core.mail import send_mail
from django.urls import reverse
from django.template.loader import render_to_string
from django.conf import settings
from bughunting.utils.tokens.bug_token_generator import bug_access_token_generator


def build_vulnerability_mail_message(vulnerability, request, hide_token=False):
    """ returns the email message for sending to domain owner """
    token = bug_access_token_generator.make_token(vulnerability)
    link = request.build_absolute_uri(
        reverse('bughunting:domain-owner:bug-details', kwargs={'pk': vulnerability.pk, 'token': token}))
    if hide_token:
        link = "LINK HIDDEN"
    message = render_to_string(
        "bughunting/mails/bug_notification_mail.html", {
            'vulnerability': vulnerability, 'link': link, 'request': request})
    return message


def send_vulnerability_mail(request, vulnerability):
    """ sending a notification mail to domain owner with link to vulnerability """
    if settings.BUGHUNTING_NEED_MANUAL_APPROVING and not vulnerability.bugmeta.approved:
        print("Submitted bug is not approved, but it is required to be published")
        return
    recipient_list = [vulnerability.bugmeta.contact_email]

    if not vulnerability.bugmeta.date_disclosure:
        vulnerability.bugmeta.date_disclosure = (timezone.now() + timezone.timedelta(
            days=settings.BUG_DISCLOSURE_DAYS)).date()
    message = build_vulnerability_mail_message(vulnerability, request)
    subject = render_to_string("bughunting/mails/bug_notification_mail_subject.html", {'vulnerability': vulnerability})
    send_mail(subject, message, None, recipient_list)
    vulnerability.bugmeta.is_sent = True
    if not vulnerability.bugmeta.date_sent:
        vulnerability.bugmeta.date_sent = timezone.now()
    vulnerability.bugmeta.save()
    vulnerability.save()
    if vulnerability.bugmeta.send_email_copy_user:
        message = build_vulnerability_mail_message(vulnerability, request, hide_token=True)
        send_mail(subject + " (copy)", message, request.user.email, [request.user.email])
