from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin
from django.conf import settings
from django.db.models import Q
from django.utils import timezone
from pinax.badges.registry import badges
from bughunting import models
from bughunting import forms
from bughunting.utils.mail.sending import send_vulnerability_mail
from opas_f_server.mixins.messages import MessageMixin


class AllBugs(generic.ListView):
    """ show all approved bugs """
    template_name = 'bughunting/pages/all_bugs_page.html'
    paginate_by = 20
    context_object_name = "bugs"
    queryset = models.Bug.objects.filter(bugmeta__approved=True).order_by('-bugmeta__date_discovered')


class SingleBug(generic.DetailView):
    template_name = 'bughunting/pages/single_bug_page.html'
    context_object_name = "bug"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = forms.UpdateBugForm()
        context['increase_date_disclosure_form'] = forms.IncreaseBugDateDisclosureForm
        if self.request.user.is_staff:
            context['approve_bug_form'] = forms.ApproveBugForm()
        if self.request.user == self.get_object().bugmeta.user:
            context['write_comment_form'] = forms.WriteCommentForm()
        return context

    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return models.Bug.objects.filter(bugmeta__approved=True, pk=self.kwargs.get('pk'), bugmeta__is_draft=False)
        return models.Bug.objects.filter(Q(bugmeta__approved=True, bugmeta__is_draft=False) | Q(
            bugmeta__user=self.request.user), pk=self.kwargs.get('pk'))


class MyBugs(LoginRequiredMixin, generic.ListView):
    template_name = "bughunting/pages/all_bugs_page.html"
    paginate_by = 20
    context_object_name = "bugs"

    def get_queryset(self):
        return models.Bug.objects.filter(bugmeta__user=self.request.user).order_by('-bugmeta__date_discovered')


class SubmitBug(LoginRequiredMixin, MessageMixin, generic.CreateView):
    form_class = forms.CreateBugForm
    template_name = "bughunting/pages/submit_bug_page.html"
    success_message = "Thanks for submitting this bug!"

    def form_valid(self, form):
        form.instance.user = self.request.user
        instance = form.save()
        bug_meta = models.BugMeta.objects.create(bug=instance,
                                                 send_email_copy_user=form.cleaned_data['send_email_copy_user'],
                                                 is_fixed=form.cleaned_data['is_fixed'], user=self.request.user,
                                                 contact_email=form.cleaned_data['contact_email'],
                                                 is_draft=form.cleaned_data['is_draft'])
        if not settings.BUGHUNTING_NEED_MANUAL_APPROVING and not bug_meta.is_draft:
            bug_meta.approved = True
            bug_meta.save()
            badges.possibly_award_badge("secured_websites_awarded", user=self.request.user)
            send_vulnerability_mail(self.request, instance)
        return super(SubmitBug, self).form_valid(form)


class ApproveBugsList(LoginRequiredMixin, generic.ListView):
    template_name = "bughunting/pages/approve_bugs_page.html"
    paginate_by = 20
    context_object_name = "bugs"

    def get_queryset(self):
        return models.Bug.objects.filter(bugmeta__approved=False, bugmeta__is_draft=False)


class UpdateBug(LoginRequiredMixin, MessageMixin, generic.UpdateView):
    form_class = forms.UpdateBugForm
    success_message = "Updated bug successfully!"

    def get_queryset(self):
        return models.Bug.objects.filter(bugmeta__user=self.request.user, pk=self.kwargs.get('pk'))

    def form_valid(self, form):
        obj = self.get_object()
        if not obj.bugmeta.is_disclosed:
            date = timezone.now().date()
            if not obj.bugmeta.is_draft:
                form.cleaned_data['is_draft'] = False
            self.handle_drafts(obj, form.cleaned_data)
            if form.cleaned_data.get('is_fixed', False):
                obj.bugmeta.date_fixed = date
                obj.bugmeta.is_fixed = True
                obj.bugmeta.is_disclosed = True
                obj.bugmeta.date_disclosed = date
            obj.bugmeta.is_draft = form.cleaned_data.get('is_draft')
            obj.bugmeta.save()
            if obj.bugmeta.is_fixed:
                badges.possibly_award_badge('patched_websites_awarded', user=self.request.user)
        return super(UpdateBug, self).form_valid(form)

    def handle_drafts(self, instance, cleaned_data):
        if instance.bugmeta.is_draft is True and not cleaned_data.get('is_draft'):
            if cleaned_data['is_draft'] is True:
                return
            if not settings.BUGHUNTING_NEED_MANUAL_APPROVING and not instance.bugmeta.is_draft:
                instance.bugmeta.approved = True
                instance.bugmeta.save()
                badges.possibly_award_badge("secured_websites_awarded", user=self.request.user)
                send_vulnerability_mail(self.request, instance)


class BugApproved(LoginRequiredMixin, MessageMixin, generic.UpdateView):
    form_class = forms.ApproveBugForm
    success_message = "Bug approved!"

    def get_queryset(self):
        if self.request.user.is_staff:
            return models.Bug.objects.filter(bugmeta__approved=False, pk=self.kwargs.get('pk'), bugmeta__is_draft=False)
        return models.Bug.objects.none()

    def form_valid(self, form):
        obj = self.get_object()
        obj.bugmeta.approved = form.cleaned_data['approved']
        obj.bugmeta.save()
        if obj.bugmeta.approved:
            send_vulnerability_mail(self.request, obj)
        return super(BugApproved, self).form_valid(form)


class IncreaseBugDateDisclosure(LoginRequiredMixin, generic.UpdateView):
    form_class = forms.IncreaseBugDateDisclosureForm
    http_method_names = ["post"]

    def get_queryset(self):
        return models.Bug.objects.filter(bugmeta__approved=True, pk=self.kwargs.get('pk'),
                                         bugmeta__is_draft=False, bugmeta__user=self.request.user)

    def form_valid(self, form):
        obj = self.get_object()
        date = obj.bugmeta.date_disclosure + timezone.timedelta(
            days=form.cleaned_data.get('days'))
        obj.bugmeta.date_disclosure = date
        obj.bugmeta.save()
        obj.save()
        return super(IncreaseBugDateDisclosure, self).form_valid(form)


class ResendNotificationMail(LoginRequiredMixin, generic.UpdateView):
    form_class = forms.ResendNotificationMailForm
    http_method_names = ["post"]

    def get_queryset(self):
        return models.Bug.objects.filter(pk=self.kwargs.get('pk'), bugmeta__approved=True, bugmeta__is_disclosed=False)

    def form_valid(self, form):
        instance = form.instance
        send_vulnerability_mail(self.request, instance)
        return super(ResendNotificationMail, self).form_valid(form)


class WriteBugComment(LoginRequiredMixin, generic.CreateView):
    form_class = forms.WriteCommentForm
    http_method_names = ["post"]

    def get_queryset(self):
        return models.Bug.objects.filter(bugmeta__user=self.request.user, pk=self.kwargs.get('pk'))

    def get_success_url(self):
        return self.get_object().get_absolute_url()

    def form_valid(self, form):
        form.instance.user = self.request.user
        form.instance.bug = self.get_object()
        return super(WriteBugComment, self).form_valid(form)
