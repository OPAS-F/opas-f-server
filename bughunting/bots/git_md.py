""" this bot needs gitpython to be installed """
import sys
import os
import git


class DisclosureBot:
    repo_url = None
    repo_dir = None
    bugs_sub_dir = None

    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

    def get_repo_url(self):
        if not self.repo_url:
            raise Exception("No repo url found")
        return self.repo_url

    def get_repo_dir(self):
        if not self.repo_dir:
            raise Exception("No repo directory found")
        return self.repo_dir

    def check_repo_exists(self):
        if not os.path.exists(self.get_repo_dir()):
            git.Repo.clone_from(self.get_repo_url(), self.get_repo_dir())
        if not os.path.exists(os.path.join(self.get_repo_dir(), self.bugs_sub_dir)):
            os.makedirs(os.path.join(self.get_repo_dir(), self.bugs_sub_dir))

    def get_filename(self, bug):
        return os.path.join(self.get_repo_dir(), self.bugs_sub_dir, bug.uuid.hex + ".md")

    def disclosed(self, message, bug):
        self.check_repo_exists()
        filename = self.get_filename(bug)
        repo = git.Repo(self.get_repo_dir())
        with open(filename, "w") as bug_file:
            bug_file.write(message)
        repo.index.add([filename])
        repo.index.commit("new bug disclosed")
        origin = repo.remote('origin')
        origin.push('master')
