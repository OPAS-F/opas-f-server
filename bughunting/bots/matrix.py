from matrix_client.client import MatrixClient
from matrix_client.api import MatrixRequestError
from requests.exceptions import MissingSchema
import sys


class DisclosureBot:
    username = None
    password = None
    host = None
    room_id = None
    display_name = None

    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

    def disclosed(self, message, bug):
        client = MatrixClient(self.host)
        try:
            client.login(username=self.username, password=self.password)
        except MatrixRequestError as e:
            print(e)
            if e.code == 403:
                print("Bad username or password.")
                sys.exit(4)
            else:
                print("Check your sever details are correct.")
                sys.exit(2)
        except MissingSchema as e:
            print("Bad URL format.")
            print(e)
            sys.exit(3)

        user = client.get_user(client.user_id)
        user.set_display_name(self.display_name)
        room = client.join_room(self.room_id)
        room.send_notice(message)
