from rest_framework import serializers
from django.utils import timezone
from django.conf import settings
from bughunting import models
from bughunting.utils.mail.sending import send_vulnerability_mail
from pinax.badges.registry import badges
from urllib.parse import urlsplit


class BaseBugMetaSerializer(serializers.ModelSerializer):
    user = serializers.StringRelatedField()

    class Meta:
        model = models.BugMeta
        fields = ('user', 'date_disclosure', 'is_disclosed', 'date_discovered', 'is_fixed', 'approved')
        read_only_fields = ('user', 'date_disclosure', 'is_disclosed', 'date_discovered', 'is_fixed', 'approved')


class FullBugMetaSerializer(serializers.ModelSerializer):
    user = serializers.StringRelatedField()

    class Meta:
        model = models.BugMeta
        exclude = ('bug',)
        read_only_fields = ('date_sent', 'date_discovered',
                            'date_disclosure', 'date_fixed', 'is_disclosed', 'approved',
                            'is_sent', 'user', 'date_disclosed')
        extra_kwargs = {
            'contact_email': {'write_only': True}
        }


class BaseBugSerializer(serializers.ModelSerializer):
    bug_info = BaseBugMetaSerializer(source='bugmeta', read_only=True)

    class Meta:
        model = models.Bug
        fields = ('uuid', 'bug_info', 'domain', 'name')
        read_only_fields = ('uuid', 'bug_info', 'domain', 'name')


class FullBugSerializer(serializers.ModelSerializer):
    bug_info = FullBugMetaSerializer(source='bugmeta')

    class Meta:
        model = models.Bug
        fields = '__all__'
        read_only_fields = ('uuid',)

    def validate_domain(self, value):
        parsed = urlsplit(value)
        return "%s://%s" % (parsed.scheme, parsed.netloc)

    def validate_method(self, value):
        return value.upper()

    def create(self, validated_data):
        info_data = validated_data.pop('bugmeta')
        info_data['user'] = self.context['request'].user
        if not settings.BUGHUNTING_NEED_MANUAL_APPROVING and not info_data.get('is_draft', False):
            info_data['approved'] = True
        bug = super(FullBugSerializer, self).create(validated_data)
        models.BugMeta.objects.create(bug=bug, **info_data)
        return bug


class IncreaseDisclosureTimeSerializer(serializers.ModelSerializer):
    days = serializers.IntegerField(min_value=1, max_value=90, write_only=True)
    bug_info = FullBugMetaSerializer(read_only=True, source='bugmeta')

    class Meta:
        model = models.Bug
        fields = ('days', 'uuid', 'domain', 'bug_info')
        read_only_fields = ('uuid', 'domain')

    def update(self, instance, validated_data):
        date = instance.bugmeta.date_disclosure + timezone.timedelta(
            days=validated_data.get('days'))
        instance.bugmeta.date_disclosure = date
        instance.bugmeta.save()
        instance.save()
        return instance
