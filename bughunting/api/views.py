from rest_framework import generics, viewsets, mixins
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import status
from pinax.badges.registry import badges
from bughunting.api import serializers
from bughunting import models
from bughunting.utils.mail.sending import send_vulnerability_mail
from opas_f_server.permissions import IsOwner


class UserBugViewSet(mixins.ListModelMixin, mixins.RetrieveModelMixin,
                     mixins.CreateModelMixin, viewsets.GenericViewSet):
    """ create a new bug, list or update users bug """
    permission_classes = [IsOwner]

    def get_serializer_class(self):
        if self.action == "resend_notification_mail":
            return None
        return serializers.FullBugSerializer

    def perform_create(self, serializer):
        obj = serializer.save()
        if obj.bugmeta.approved:
            badges.possibly_award_badge("secured_websites_awarded", user=self.request.user)
            send_vulnerability_mail(self.request, obj)

    def get_queryset(self):
        return models.Bug.objects.filter(bugmeta__user=self.request.user)

    @action(methods=['post'], detail=True, permission_classes=[IsOwner], url_path="resend-notification-mail")
    def resend_notification_mail(self, request, pk=None):
        obj = self.get_object()
        data = {'success': False}
        if not obj.bugmeta.is_disclosed:
            send_vulnerability_mail(self.request, obj)
            data['success'] = True
        return Response(data, status=status.HTTP_200_OK)


class BugsViewSet(mixins.ListModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    """ list or retrieve bug. show different information on disclosure state """
    serializer_class = serializers.BaseBugSerializer

    def get_serializer_class(self):
        if self.action == "list":
            return serializers.BaseBugSerializer
        instance = self.get_object()
        if instance.bugmeta.is_disclosed or self.request.user == instance.bugmeta.user:
            return serializers.FullBugSerializer
        return serializers.BaseBugSerializer

    def get_queryset(self):
        return models.Bug.objects.filter(bugmeta__is_sent=True).order_by(
            '-bugmeta__date_discovered')


class IncreaseDisclosureTime(generics.UpdateAPIView):
    """ increase disclosure deadline of a bug owned by the user"""
    serializer_class = serializers.IncreaseDisclosureTimeSerializer
    permission_classes = [IsOwner]

    def get_queryset(self):
        return models.Bug.objects.filter(pk=self.kwargs.get('pk'), bugmeta__is_disclosed=False)
