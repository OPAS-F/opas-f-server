from django.urls import path
from django.conf import settings
from rest_framework.routers import DefaultRouter
from bughunting.api import views

app_name = 'bughunting'

router = DefaultRouter()
router.register(r'user-bugs', views.UserBugViewSet, basename='user-bug')
router.register(r'bugs', views.BugsViewSet, basename='bug')

urlpatterns = [
    path('bugs/<str:pk>/disclosure-deadline', views.IncreaseDisclosureTime.as_view(), name='increase-disclosure-deadline'),
]

urlpatterns += router.urls
