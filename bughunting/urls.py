from django.urls import path, include
from bughunting import views


app_name = "bughunting"


urlpatterns = [
    path('bugs/', views.AllBugs.as_view(), name="all_bugs"),
    path('bugs/my/', views.MyBugs.as_view(), name="my-bugs"),
    path('bugs/submit/', views.SubmitBug.as_view(), name='submit-bug'),
    path('bugs/approval/', views.ApproveBugsList.as_view(), name='bugs-approval-list'),
    path('bugs/<str:pk>/', views.SingleBug.as_view(), name="single-bug"),
    path('bugs/<str:pk>/comments/new/', views.WriteBugComment.as_view(), name="write-comment"),
    path('bugs/<str:pk>/edit/', views.UpdateBug.as_view(), name='edit-bug'),
    path('bugs/<str:pk>/approve/', views.BugApproved.as_view(), name='approve-bug'),
    path('bugs/<str:pk>/increase-date-disclosure/',
         views.IncreaseBugDateDisclosure.as_view(), name='increase-date-disclosure'),
    path('bugs/<str:pk>/resend-notification-mail/', views.ResendNotificationMail.as_view(),
         name='resend-notification-mail'),

    path('bugs/token-authentication/', include('bughunting.domain_owner.urls')),
]
