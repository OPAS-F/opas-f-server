from django.contrib import admin
from bughunting import models
# Register your models here.


class BugMetaAdminInline(admin.TabularInline):
    model = models.BugMeta


class BugAdmin(admin.ModelAdmin):
    inlines = [BugMetaAdminInline]


admin.site.register(models.Bug, BugAdmin)
admin.site.register(models.Comment)
