from django.apps import AppConfig


class BughuntingConfig(AppConfig):
    name = 'bughunting'
