from django import forms
from bughunting import models


class CreateBugForm(forms.ModelForm):
    contact_email = forms.EmailField()
    is_fixed = forms.BooleanField(required=False)
    is_draft = forms.BooleanField(required=False)
    send_email_copy_user = forms.BooleanField(required=False)

    class Meta:
        model = models.Bug
        exclude = ('uuid', )


class UpdateBugForm(forms.ModelForm):
    is_fixed = forms.BooleanField(required=False)
    is_draft = forms.BooleanField(required=False)

    class Meta:
        model = models.Bug
        fields = ['is_fixed', 'is_draft']


class ApproveBugForm(forms.ModelForm):
    approved = forms.BooleanField()

    class Meta:
        model = models.Bug
        fields = ['approved']


class IncreaseBugDateDisclosureForm(forms.ModelForm):
    days = forms.IntegerField(min_value=1, max_value=90)

    class Meta:
        model = models.Bug
        fields = ['days']


class WriteCommentForm(forms.ModelForm):
    class Meta:
        model = models.Comment
        fields = ['text']


class ResendNotificationMailForm(forms.ModelForm):
    class Meta:
        model = models.Bug
        fields = []
