from django.urls import path, include
from accounts import views


app_name = "accounts"


urlpatterns = [
    path('login/', views.LoginView.as_view(), name="login_view"),
    path('signup/', views.SignupView.as_view(), name='signup_view'),
    path('activate/<str:uidb64>/<str:token>/', views.ActivateAccount.as_view(), name='activate-account'),
    path('logout/', views.LogoutView.as_view(), name="logout_view"),
    path('dashboard/', include('accounts.dashboard.urls')),
    path('settings/', views.UpdateSettings.as_view(), name='settings'),
    path('profile/<int:pk>/', views.ProfileView.as_view(), name='profile'),
    path('profile/edit/', views.EditProfileView.as_view(), name='edit-profile'),
]
