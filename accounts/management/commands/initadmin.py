from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
import os


class Command(BaseCommand):
    help = "create admin if non exists"

    def handle(self, *args, **options):
        if User.objects.count() == 0:
            username = os.environ.get('ADMIN_USERNAME')
            password = os.environ.get('ADMIN_PASSWORD')
            email = os.environ.get('ADMIN_EMAIL')
            if not username or not password:
                raise CommandError("Need username and password")
            User.objects.create_superuser(username, email, password)
            self.stdout.write("Created admin user!")
