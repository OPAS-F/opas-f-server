from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils import six


class VerifyAccountTokenGenerator(PasswordResetTokenGenerator):
    def _make_hash_value(self, user , timestamp):
        return (
            six.text_type(user.pk) + six.text_type(timestamp) + six.text_type(user.is_active) +
            six.text_type(user.profile.email_confirmed) + six.text_type(user.email)
        )


verify_account_token_generator = VerifyAccountTokenGenerator()
