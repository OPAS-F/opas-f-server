# Generated by Django 2.1.7 on 2019-03-15 06:54

from django.conf import settings
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('accounts', '0005_accountsettings_public_profile'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='accepteddonation',
            unique_together={('key', 'value', 'user')},
        ),
    ]
