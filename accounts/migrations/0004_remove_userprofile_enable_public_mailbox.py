# Generated by Django 2.1.5 on 2019-02-04 09:50

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0003_accountsettings_bughunting_email_submit'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='userprofile',
            name='enable_public_mailbox',
        ),
    ]
