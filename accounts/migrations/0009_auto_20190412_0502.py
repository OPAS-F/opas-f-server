# Generated by Django 2.1.7 on 2019-04-12 05:02

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0008_auto_20190412_0458'),
    ]

    operations = [
        migrations.RenameField(
            model_name='userprofile',
            old_name='bio',
            new_name='about_me',
        ),
    ]
