from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from pentesting import models as pentesting_models


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile')
    about_me = models.TextField(blank=True)
    contact_details = models.TextField(blank=True, null=True)
    email_confirmed = models.BooleanField(default=False)
    public_key = models.TextField(blank=True, null=True)
    prefered_bug_bounty = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.user.username

    @property
    def vulnerability_count(self):
        return self.user.vulnerability_set.count()

    @property
    def host_count(self):
        return pentesting_models.Host.objects.filter(project__userproject__user=self.user).count()

    @property
    def service_count(self):
        return pentesting_models.Service.objects.filter(host__project__userproject__user=self.user).count()

    @property
    def credential_count(self):
        return pentesting_models.Credential.objects.filter(service__host__project__userproject__user=self.user).count()

    class Meta:
        verbose_name = "User Profile"
        verbose_name_plural = "User Profiles"


class AccountSettings(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    bughunting_email_submit = models.BooleanField(default=False)
    public_profile = models.BooleanField(default=True)
    # enable_public_mailbox = models.BooleanField(default=False)

    def __str__(self):
        return self.user.username

    class Meta:
        verbose_name = "Account Settings"
        verbose_name_plural = "Account Settings"


def create_user_profile(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.create(user=instance)


def create_account_settings(sender, instance, created, **kwargs):
    if created:
        AccountSettings.objects.create(user=instance)


post_save.connect(create_user_profile, sender=User)
post_save.connect(create_account_settings, sender=User)
