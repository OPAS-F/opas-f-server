from django.urls import path, include

app_name = "accounts"

urlpatterns = [
    path('o/', include('oauth2_provider.urls')),
    path('dashboard/', include('accounts.dashboard.api.urls')),
]
