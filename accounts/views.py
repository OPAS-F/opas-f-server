from django.contrib.auth import views as auth_views
from django.contrib.auth.models import User
from django.views import generic
from django.contrib import messages
from django.conf import settings
from django.urls import reverse_lazy
from django.shortcuts import get_object_or_404
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import Http404
from accounts import forms
from accounts import models
from accounts.utils import signup
from bughunting.models import Bug


class LoginView(auth_views.LoginView):
    """ login user """
    template_name = 'accounts/login_page.html'
    success_url = reverse_lazy('accounts:dashboard')
    redirect_authenticated_user = True


class LogoutView(auth_views.LogoutView):
    """ logout user """
    pass


class UpdateSettings(LoginRequiredMixin, generic.UpdateView):
    form_class = forms.SettingsForm
    template_name = 'accounts/pages/settings.html'

    def get_object(self, queryset=None):
        if queryset is None:
            queryset = self.get_queryset()
        return get_object_or_404(queryset)

    def get_queryset(self):
        return models.AccountSettings.objects.filter(user=self.request.user)

    def get_success_url(self):
        return reverse_lazy('accounts:dashboard')


class EditProfileView(LoginRequiredMixin, generic.UpdateView):
    template_name = 'accounts/pages/edit_profile_page.html'
    form_class = forms.EditProfileForm

    def get_object(self, queryset=None):
        if queryset is None:
            queryset = self.get_queryset()
        try:
            obj = queryset.get()
        except queryset.model.DoesNotExist:
            raise Http404('No profile found matching the query')
        return obj

    def get_success_url(self):
        return reverse_lazy('accounts:profile', kwargs={'pk': self.request.user.pk})

    def get_queryset(self):
        return models.UserProfile.objects.filter(user=self.request.user)


class ProfileView(generic.DetailView):
    template_name = 'accounts/pages/profile.html'

    def get_context_data(self, **kwargs):
        context = super(ProfileView, self).get_context_data(**kwargs)
        context['reported_bugs'] = Bug.objects.filter(bugmeta__user__pk=self.kwargs.get('pk'), bugmeta__approved=True)
        return context

    def get_queryset(self):
        return User.objects.filter(pk=self.kwargs.get('pk'), accountsettings__public_profile=True)


class SignupView(generic.FormView):
    template_name = "accounts/pages/signup.html"
    form_class = forms.SignupForm
    success_url = reverse_lazy('accounts:login_view')

    def get_context_data(self, **kwargs):
        context = super(SignupView, self).get_context_data(**kwargs)
        context['REGISTRATION_ENABLED'] = settings.ENABLE_REGISTRATION
        return context

    def form_valid(self, form):
        if not settings.ENABLE_REGISTRATION:
            # do nothing, signup disabled
            return super(SignupView, self).form_valid(form)
        user = User(username=form.cleaned_data.get('username'))
        user.set_password(form.cleaned_data.get('password'))
        user.save()
        if settings.REQUIRE_SIGNUP_EMAIL_CONFIRM:
            user.is_active = False
            user.email = form.cleaned_data.get('email')
            user.save()
            signup.prepare_signup_email(self.request, user)
        return super(SignupView, self).form_valid(form)


class ActivateAccount(generic.RedirectView):
    url = reverse_lazy('accounts:login_view')

    def get(self, request, *args, **kwargs):
        uidb64 = self.kwargs.get('uidb64')
        token = self.kwargs.get('token')
        result, user = signup.activate_account_check(uidb64, token)
        if result:
            user.is_active = True
            user.save()
            messages.add_message(request, messages.SUCCESS, "Account activated!")
        else:
            messages.add_message(request, messages.ERROR, "Account activation failed!")
        return super(ActivateAccount, self).get(request, *args, **kwargs)
