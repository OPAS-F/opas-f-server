from django.urls import path
from accounts.dashboard import views

app_name = "dashboard"

urlpatterns = [
    path('', views.Dashboard.as_view(), name="index")
]
