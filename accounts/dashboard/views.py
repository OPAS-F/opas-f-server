from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin
from bughunting.models import Bug


class Dashboard(LoginRequiredMixin, generic.TemplateView):
    template_name = "accounts/dashboard.html"

    def get_context_data(self, **kwargs):
        context = super(Dashboard, self).get_context_data(**kwargs)
        context['latest_submissions'] = Bug.objects.filter(bugmeta__user=self.request.user, bugmeta__approved=True)[:5]
        return context
