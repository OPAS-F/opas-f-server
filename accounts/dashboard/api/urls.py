from django.urls import path
from accounts.dashboard.api import views

app_name = "dashboard"


urlpatterns = [
    path('fixed-unfixed-bugs/', views.PatchedUnpatchedStatistic.as_view(), name="fixed-unfixed-user-bugs")
]
