from rest_framework import generics
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from bughunting.models import Bug


class PatchedUnpatchedStatistic(generics.RetrieveAPIView):
    permission_classes = [IsAuthenticated]

    def retrieve(self, request, *args, **kwargs):
        bugs = Bug.objects.filter(bugmeta__approved=True, bugmeta__user=self.request.user)
        patched = bugs.filter(bugmeta__is_fixed=True).count()
        unpatched = bugs.filter(bugmeta__is_fixed=False, bugmeta__is_disclosed=True).count()
        awaiting_response = bugs.filter(bugmeta__is_disclosed=False).count()
        data = {'patched': patched, 'unpatched': unpatched, 'awaiting_response': awaiting_response}
        return Response(data)
