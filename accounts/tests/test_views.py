from django.test import TestCase
from django.contrib.auth.models import User
from django_dynamic_fixture import G
from django.urls import reverse_lazy


class AccountViewsTest(TestCase):
    def setUp(self):
        self.user = G(User, password="test")

    def test_login(self):
        data = {'username': self.user.username, 'password': 'test'}
        url = reverse_lazy('accounts:login_view')
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(self.user.is_authenticated, True)

    def test_logout(self):
        # TODO: implement
        pass

    def test_dashboard(self):
        # TODO: implement
        pass

    def test_update_account_settings(self):
        # TODO: implement
        pass

    def test_edit_profile(self):
        # TODO: implement
        pass

    def test_profile_view(self):
        # TODO: implement
        pass

    def test_signup(self):
        # TODO: implement
        pass

    def test_activate_account(self):
        # TODO: implement
        pass
