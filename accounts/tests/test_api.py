from rest_framework.test import APITestCase
from rest_framework import status
from django_dynamic_fixture import G
from django.contrib.auth.models import User
from django.urls import reverse
from django.test import override_settings


class AccountViewsTests(APITestCase):

    def setUp(self):
        self.user = G(User)
        self.user.set_password("password")
        self.user.save()
        self.client.force_login(user=self.user)

    # BLOCKED BY: https://github.com/encode/django-rest-framework/issues/6030
    #@override_settings(ENABLE_REGISTRATION=True)
    #def test_signup(self):
    #    self.settings().reload()
    #    with override_settings(ENABLE_REGISTRATION=True):
    #        url = reverse('api:accounts:signup')
    #        data = {'username': "testuser", "password": "password"}
    #        response = self.client.post(url, data)
    #        self.assertEqual(response.status_code, 201)
    #        self.assertEqual(User.objects.count(), 2)

    #def test_signup_forbidden(self):
    #    self.modify_settings(ENABLE_REGISTRATION=True)
    #    try:
    #        url = reverse('api:accounts:signup')
    #        data = {'username': 'testuser', 'password': 'password'}
    #        response = self.client.post(url, data)
    #        self.assertEqual(response.status_code, 404)
    #    except:
    #        pass

    def test_account_settings(self):
        pass

    def test_account_settings_forbidden(self):
        pass

    def test_profile_detail(self):
        pass

    def test_profile_detail_forbidden(self):
        pass

    def test_user_profile(self):
        pass

    def test_user_profile_forbidden(self):
        pass

    def test_signup_email_confirm(self):
        pass

    def test_activate_account(self):
        pass

    def test_activate_account_forbidden(self):
        pass
