from django import forms
from django.contrib.auth.models import User
from django.contrib.auth import password_validation
from accounts import models


class SettingsForm(forms.ModelForm):
    class Meta:
        model = models.AccountSettings
        exclude = ['user']


class SignupForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)
    password1 = forms.CharField(widget=forms.PasswordInput, label="Password (confirm)")

    class Meta:
        model = User
        fields = ['username', 'password', 'password1', 'email']

    def validate_password(self, value):
        password_validation.validate_password(value)
        return value

    def clean(self):
        if self.cleaned_data.get('password') != self.cleaned_data.get('password1'):
            raise forms.ValidationError("Passwords do not match!")


class EditProfileForm(forms.ModelForm):
    class Meta:
        model = models.UserProfile
        fields = ['about_me', 'prefered_bug_bounty', 'contact_details']
