from django.conf import settings
from django.utils.encoding import force_text
from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
from django.utils.http import urlsafe_base64_decode
from django.utils.http import urlsafe_base64_encode
from django.utils.encoding import force_bytes
from accounts.tokens import verify_account_token_generator
from django.core.mail import send_mail
from django.contrib.auth.models import User


def prepare_signup_email(request, user):
    current_site = get_current_site(request)
    subject = settings.SIGNUP_ACTIVATION_EMAIL_SUBJECT
    message = render_to_string('accounts/mails/signup_email.html', {
        'user': user,
        'domain': current_site.domain,
        'uid': urlsafe_base64_encode(force_bytes(user.pk)).decode('utf-8'),
        'token': verify_account_token_generator.make_token(user),
    })
    send_mail(
        subject, message,
        settings.DEFAULT_FROM_EMAIL, [user.email], fail_silently=False)


def activate_account_check(uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and verify_account_token_generator.check_token(user, token):
        user.profile.email_confirmed = True
        user.profile.save()
        return True, user
    return False, None
