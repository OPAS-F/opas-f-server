from rest_framework import generics
from rest_framework.response import Response

# Create your views here.


class CheckStatus(generics.RetrieveAPIView):
    """ view for let client check if alive """

    def retrieve(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return Response({'status': 'ok', 'auth': True})
        return Response({'status': 'ok', 'auth': False})
