from django.urls import path, include
from django.conf import settings
from api import views

app_name = "api"

urlpatterns = [
    path('accounts/', include('accounts.api.urls')),
    path('pentesting/', include('pentesting.urls')),
    path('status/check', views.CheckStatus.as_view(), name='check-status'),
    path('public/', include('public.api.urls')),
]

if settings.ENABLE_BUGHUNTING_APPLICATION:
    urlpatterns += [
        path('bughunting/', include('bughunting.api.urls')),
    ]

if settings.ENABLE_BLOGGING_APPLICATION:
    urlpatterns += [
        path('blogs/', include('blogging.urls'))
    ]
