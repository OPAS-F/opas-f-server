"""
Template for local settings
"""


# -----------------------------
# Users and Accounts
# -----------------------------
REGISTRATION_ENABLED = False
REQUIRE_SIGNUP_EMAIL = True
SIGNUP_CAPTCHA = True
SIGNUP_ACTIVATION_EMAIL_SUBJECT = "Activate your CySec Account"


# -----------------------------
# Administration
# -----------------------------
ENABLE_ADMIN_INTERFACE = True
ADMIN_URL_PATH = "admin/"


# -----------------------------
# E-Mail
# -----------------------------
EMAIL_HOST = 'smtp.riseup.net'
EMAIL_PORT = 587
EMAIL_HOST_USER = "john.doe"
EMAIL_HOST_PASSWORD = "averylongandsecurepassword"
EMAIL_USE_TLS = True
EMAIL_USE_SSL = False
DEFAULT_FROM_EMAIL = "john.doe@riseup.net"


# -----------------------------
# Bughunting application
# -----------------------------
ENABLE_BUGHUNTING_APPLICATION = True
REPORTS_HTML_TEMPLATES = [
    ('reports/default.html', 'Default HTML-Template')
]
BUG_DISCLOSURE_BOTS = [
    {
        'name': 'matrix', 'enabled': False,
        'module': 'bughunting.bots.matrix', 'args': {
            'username': None, 'password': None,
            'host': 'https://matrix.org', 'room_id': '!gyerWVOrbOhlSNdEEn:matrix.org',
            'display_name': 'OPAS-F [Bot]', 'message_template': 'bots/matrix.html'
        }
    }
]
BUG_DISCLOSURE_DAYS = 30
BUGHUNTING_NEED_MANUAL_APPROVING = False

# -----------------------------
# Blogging application
# -----------------------------
ENABLE_BLOGGING_APPLICATION = True


# -----------------------------
# Misc
# -----------------------------
# needed for automatic bug disclosure script
SELF_DOMAIN = "http://localhost:8000"
PASSWORD_RESET_TIMEOUT_DAYS = 14
DEBUG = False
ALLOWED_HOSTS = []


# -----------------------------
# Privacy
# -----------------------------
import socks
import smtplib
socks.setdefaultproxy(socks.SOCKS5, 'localhost', 9050)
socks.wrapmodule(smtplib)
