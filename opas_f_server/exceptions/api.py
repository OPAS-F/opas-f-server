from rest_framework import exceptions
from rest_framework import status


class ForbiddenAPIException(exceptions.APIException):
    status_code = status.HTTP_403_FORBIDDEN
    default_code = 'error'

    def __init__(self, detail="forbidden", status_code=None):
        self.detail = detail
        if status_code is not None:
            self.status_code = status_code
