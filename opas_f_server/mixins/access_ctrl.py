from django.contrib.auth.mixins import UserPassesTestMixin


class IsOwnerMixin(UserPassesTestMixin):

    permission_denied_message = 'Permission denied!'

    def test_func(self):
        if self.request.user == self.get_object().owner:
            return True
        return False


class StaffRequiredMixin(UserPassesTestMixin):
    pass
