from django.contrib import messages


class SuccessMessageMixin(object):

    success_message = ''

    def get_success_message(self, cleaned_data):
        return self.success_message % cleaned_data

    def form_valid(self, form):
        response = super().form_valid(form)
        success_message = self.get_success_message(form.cleaned_data)
        if success_message:
            messages.success(self.request, success_message)
        return response


class ErrorMessageMixin(object):
    error_message = 'Oops something went wrong: %s'

    def get_error_message(self, cleaned_data):
        return self.error_message % cleaned_data

    def form_invalid(self, form):
        response = super().form_invalid(form)
        if form.errors:
            for error in form.errors.get('__all__'):
                error_message = self.get_error_message(error)
                messages.error(self.request, error_message)
        return response


class MessageMixin(SuccessMessageMixin, ErrorMessageMixin):
    pass
