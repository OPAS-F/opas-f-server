# Open Pentesting and Security Framework


This is the server part of the Open Pentesting and Security Framework.


*This is an early beta state and may contain bugs*


Please report bugs and other ideas through the [Bug-Tracker](https://codeberg.org/OPAS-F/opas-f-server/issues)


# Setup & Usage
Our Documentation can be found at our [homepage](https://opasf.github.io/docs/)


# Features

- Pentesting
    - Unlimited amount of pentesting projects
    - Add other users to your project and specify a role like "pentester" or "project admin"
    - Tasks, which can optionally be assigned to a user
    - create pentesting reports based on the discovered information and vulnerabilities
    - optionally encrypt reports using AES-GCM or ChaCha20Poly1305 (server-side)
    - Master-Slave architecture (TODO: describe bit more, see doc at wiki)

- Bug Bounty
    - Bug Hunters can win awards for securing websites
    - diclose vulnerabilities within 30 days (can be increased using settings)
    - bug hunter can increase per bug dislosure deadline, if admin needs more time to fix it
    - store discovered bugs as draft
    - show bug details only using sharable link, which prevent the need for domain administration to create accounts on the server
    - details of not disclosed or fixed vulnerabilities are hidden for other users
    - sending email to site admins for vulnerability notification with access link
    - Bots, that notify users on social medias about disclosed bugs

- General
    - Disable / Enable registration of new accounts
    - User Profiles (Accessable without beeing authenticated by default)
    - User Blogs (beta; with markdown support)

- Built-In Bug disclosure Bots:
    - Matrix


there are more features planned. For more information have a look at the issues page or
our [blog](https://opasf.github.io).
